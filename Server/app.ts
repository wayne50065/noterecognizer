import dotenv from 'dotenv';
dotenv.config();
//
import express from 'express';
// import {Request,Response} from 'express';
import multer from 'multer';
import grant from 'grant';
import Knex from 'knex';
// import { Client } from 'pg';
import { storage, storageProtected } from './utils/storage';
import expressSession from 'express-session';
import { isLoggedIn } from './utils/guard';
import { logger } from './utils/loggers';
import { UserService } from './service/UserService';
import { UserController } from './controller/UserController';
import { CashService } from './service/CashService';
import { CashController } from './controller/CashController';



export const upload = multer({ storage: storage });
export const uploadProtected = multer({ storage: storageProtected });


const app = express();

const knexConfigs = require('./knexfile');
const environment = process.env.NODE_ENV || 'development';
const knexConfig = knexConfigs[environment];
const knex = Knex(knexConfig);


//console.log(process.env.DB_USERNAME)
// export const client = new Client({
//     database: process.env.DB_NAME,
//     user: process.env.DB_USERNAME,
//     password: process.env.DB_PASSWORD,
//   });
// client.connect();

app.use(express.json());
app.use(express.urlencoded({ extended: true })); 
app.use(
    expressSession({
      secret: 'noterecongizer',
      resave: true,
      saveUninitialized: true,
    })
  );

//new add - start


const grantExpress = grant.express({
  defaults: {
    origin: 'http://localhost:8080',
    transport: 'session',
    state: true,
  }
});

app.use(grantExpress);
app.use((req, res, next) => {
  logger.debug(`req path: ${req.path}, method: ${req.method}`);
  next();
});


export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const cashService = new CashService(knex);
export const cashController = new CashController(cashService);


import { routes } from './routes/routes';
app.use('/',routes);


app.use(express.static('public'));
app.use(isLoggedIn, express.static('protected'));
  
app.use((req, res) => {
    res.redirect('/404.html');
});
// test

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});