-- DROP TABLE IF EXISTS cash;

DROP TABLE IF EXISTS cash_result;
DROP TABLE IF EXISTS cash_category;
DROP TABLE IF EXISTS cash_image;
DROP TABLE IF EXISTS user_info;


CREATE TABLE user_info (
    id SERIAL primary key,
    emailaddress TEXT UNIQUE NOT NULL,
    password TEXT,
    created_at TIMESTAMP DEFAULT NOW()::timestamp,
    updated_at TIMESTAMP DEFAULT NOW()::timestamp
);

CREATE TABLE cash_category (
    id SERIAL primary key,
    currency TEXT,
    face_value INTEGER NOT NULL,
    category Text,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE cash_image (
    id SERIAL primary key,
    user_id INTEGER NOT NULL,
    cash_image TEXT,
    batch_no INTEGER,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES user_info(id)
);

CREATE TABLE cash_result (
    id SERIAL primary key,
    user_id INTEGER NOT NULL,
    cash_image_id INTEGER NOT NULL,
    cash_category_id INTEGER,
    quantity INTEGER,
    accuracy FLOAT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES user_info(id),
    FOREIGN KEY (cash_image_id) REFERENCES cash_image(id),
    FOREIGN KEY (cash_category_id) REFERENCES cash_category(id)
);


insert into cash_category (currency, face_value, category)values('USD', 10, 'USD10');
insert into cash_category (currency, face_value, category)values('USD', 20, 'USD20');
insert into cash_category (currency, face_value, category)values('HKD', 20, 'HKD20');
insert into cash_category (currency, face_value, category)values('HKD', 100, 'HKD100');
insert into cash_category (currency, face_value, category)values('HKD', 10, 'HKD10');


insert into cash_result (user_id,  cash_category_id, quantity, accuracy, cash_image_id)values(
(select id from user_info WHERE emailaddress = 'testing0815@gmail.com'),

(select id from cash_category WHERE category = 'USD10'),
1,
0.51,
(select id from cash_image WHERE cash_image = 'image_id-1631960142654.jpeg')
    );


insert into cash_result (user_id,  cash_category_id, quantity, accuracy, cash_image_id)values(
(select id from user_info WHERE emailaddress = 'mark@tecky.io'),

(select id from cash_category WHERE category = 'USD10'),
1,
0.51,
(select id from cash_image WHERE cash_image = 'image_id-1631798950096.jpeg')
    );

insert into cash_result (user_id,  cash_category_id, quantity, accuracy, cash_image_id)values(
    (select id from user_info WHERE emailaddress = 'testing0815@gmail.com'),
    
    (select id from cash_category WHERE category = 'HKD20'),
    1,
    0.99,
    (select id from cash_image WHERE cash_image = 'image_id-1631798950160.jpeg')
    );

insert into cash_result (user_id,  cash_category_id, quantity, accuracy, cash_image_id)values(
    (select id from user_info WHERE emailaddress = 'testing0815@gmail.com'),
    
    (select id from cash_category WHERE category = 'HKD100'),
    1,
    0.99,
    (select id from cash_image WHERE cash_image = 'image_id-1631798950160.jpeg')
    );    


select * from user_info;
select * from cash_category;
select * from cash_image;
select * from cash_result;

select max(batch_no) from cash_image;




select 
user_info.emailaddress as emailaddress,
cash_image.user_id as userid,
cash_image.cash_image as cash_image,
cash_image.batch_no,
json_agg(cash_category.currency) as currency,
json_agg(cash_category.face_value) as face_value,
json_agg(cash_result.quantity) as quantity,
json_agg(cash_result.accuracy) as accuracy,
cash_image.created_at as images_created_at,
cash_image.updated_at as images_updated_at,
cash_result.created_at as result_created_at,
cash_result.updated_at as result_updated_at
from cash_image INNER JOIN user_info on cash_image.user_id=user_info.id 
INNER JOIN cash_result on cash_image.id=cash_result.cash_image_id
LEFT JOIN cash_category on cash_category.id=cash_result.cash_category_id 
WHERE user_info.emailaddress = 'testing0815@gmail.com'
group by user_info.emailaddress,cash_image.cash_image,cash_image.user_id,cash_image.batch_no,cash_image.created_at,
cash_image.updated_at,
cash_result.created_at,
cash_result.updated_at
 

select 
user_info.emailaddress as emailaddress,
cash_image.user_id as userid,
cash_image.cash_image as cash_image,
cash_image.batch_no,
cash_result.accuracy as accuracy,
cash_category.currency as currency,
cash_category.face_value as face_value,
cash_result.quantity as quantity,
cash_result.created_at as result_created_at
from cash_image INNER JOIN user_info on cash_image.user_id=user_info.id 
INNER JOIN cash_result on cash_image.id=cash_result.cash_image_id
LEFT JOIN cash_category on cash_category.id=cash_result.cash_category_id 
WHERE user_info.emailaddress = 'testing0815@gmail.com'


select 
      user_info.emailaddress as emailaddress,
      cash_image.user_id as userid,
      cash_image.cash_image as cash_image,
      cash_image.batch_no,
      cash_result.accuracy as accuracy,
      cash_category.currency as currency,
      cash_category.face_value as face_value,
      cash_result.quantity as quantity,
      cash_result.created_at as result_created_at
      from cash_image INNER JOIN user_info on cash_image.user_id=user_info.id 
      INNER JOIN cash_result on cash_image.id=cash_result.cash_image_id
      LEFT JOIN cash_category on cash_category.id=cash_result.cash_category_id 
      WHERE user_info.emailaddress = 'mark@tecky.io'