DROP TABLE IF EXISTS cash_result;
DROP TABLE IF EXISTS cash_category;
DROP TABLE IF EXISTS cash_image;
DROP TABLE IF EXISTS user_info;


CREATE TABLE user_info (
    id SERIAL primary key,
    emailaddress TEXT UNIQUE NOT NULL,
    password TEXT,
    create_at TIMESTAMP,
    update_at TIMESTAMP
);

CREATE TABLE cash_category (
    id SERIAL primary key,
    currency TEXT,
    face_value INTEGER NOT NULL,
    create_at TIMESTAMP,
    update_at TIMESTAMP
);

CREATE TABLE cash_image (
    id SERIAL primary key,
    user_id INTEGER NOT NULL,
    cash_image_id TEXT,
    create_at TIMESTAMP,
    update_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES user_info(id)
);

CREATE TABLE cash_result (
    id SERIAL primary key,
    user_id INTEGER NOT NULL,
    cash_image_id INTEGER NOT NULL,
    cash_category_id INTEGER,
    quantity INTEGER,
    create_at TIMESTAMP,
    update_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES user_info(id),
    FOREIGN KEY (cash_image_id) REFERENCES cash_image(id),
    FOREIGN KEY (cash_category_id) REFERENCES cash_category(id)
);


select * from user_info ;
select * from cash_category;
select * from cash_image;
select * from cash_result;

