
const loginModal = new bootstrap.Modal(document.querySelector("#loginModal"), {
    keyboard: false
})
const logoutModal = new bootstrap.Modal(document.querySelector("#logoutModal"), {
    keyboard: false
})
const login_btn=document.querySelector('.login-form-btn')
const register_btn=document.querySelector('.register-form-btn')
const login_modal=document.querySelector("#loginModal")
const modal_body=document.querySelector('.modal-body')
const login_form=document.querySelector(".login-form-container")
const register_form=document.querySelector(".register-form-container")
const start_container=document.querySelector(".start-container")
const top_bar=document.querySelector(".top-bar-container")
const logo_banner=document.querySelector(".logo-banner-container")
const app_logo=document.querySelector(".app-logo")
const menu_btn=document.querySelector(".menu-btn-container .menu-btn")
const menu_list=document.querySelector(".drop-down-meun")
const login_icon=document.querySelector(".user-loggedin-icon")
const logout_icon=document.querySelector(".user-icon")
const image_container=document.querySelector('.image-container')
const preview_image_id = document.querySelector('#image_id')
const preview_images = document.querySelector('#preview')
const upload_container=document.querySelector('.upload-bar-container')


preview_image_id.onclick=()=>{
    preview_images.innerHTML=""

}

login_icon.onclick=()=>{
    logoutModal.show()
}

menu_btn.onclick=()=>{
    menu_list.classList.toggle('display-none')
}

login_btn.onclick=()=>{
    modal_body.classList.remove('display-none')
    login_btn.classList.add('active')
    register_btn.classList.remove('active')
    login_form.classList.remove('display-none')
    register_form.classList.add('display-none')
}

register_btn.onclick=()=>{
    modal_body.classList.remove('display-none')
    register_btn.classList.add('active')
    login_btn.classList.remove('active')
    register_form.classList.remove('display-none')
    login_form.classList.add('display-none')
}




document.querySelector("#register-form").onsubmit = async function (event) {
    event.preventDefault()

    const form = event.target
    const formObj = {

        emailaddress: form.emailaddress.value,
        password: form.password.value
    }

    const res = await fetch('/registerform', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObj)
    })
    const result = await res.json();


    if (res.status === 200) {
        window.location = '/index.html'
    }else {
        const fail = document.querySelector('#reigster-fail')
        if (fail.childElementCount == 0) {
            fail.innerHTML += `<div class="alert alert-danger" role="alert">
            Register fail! ${result.msg}
        </div>`
        }

    }

}


document.querySelector('#login-form').onsubmit = async function (event) {
    event.preventDefault();

    const form = event.target;
    const formObj = {

        emailaddress: form.emailaddress.value,
        password: form.password.value
    }

    const res = await fetch('/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObj)
    })

    const result = await res.json();
    if (res.status === 200) {
        window.location = '/index.html'


    } else {
        const fail = document.querySelector('#login-fail')
        if (fail.childElementCount == 0) {
            fail.innerHTML += `<div class="alert alert-danger" role="alert">
            Login fail! ${result.msg}
        </div>`
        }

    }
}

document.querySelector("#cash-form").addEventListener("submit", async function (event) {
    event.preventDefault()

    console.log(event);
    const form = event.target
    console.log(form);
    const formData = new FormData();
    
    
    formData.append("image_id", form.image_id.files[0]);
    formData.append("image_id", form.image_id.files[1]);
    formData.append("image_id", form.image_id.files[2]);
    formData.append("image_id", form.image_id.files[3]);
    formData.append("image_id", form.image_id.files[4]);
    formData.append("image_id", form.image_id.files[5]);


    console.log("formData");
    console.log(formData["image_id"]);
    const res = await fetch("/cashForm", {
        method: "POST",
        body: formData,
    });

    const aiResults = await res.json();

    aiResultContainer=document.querySelector('#preview');
    preview_images.innerHTML=""
    for (i in aiResults.aiResults){
        let accuracy = (aiResults.aiResults[i].probability)*100 +"%";
        aiResultContainer.innerHTML+=
        `<div class="uploaded-img"><img src="/uploads/${aiResults.aiResults[i].imageName}" class=""/> 
        <div class="info-cards-text">Currency: ${aiResults.aiResults[i].class}  Accuracy: ${accuracy}</div>
        <br/>
        </div> 
        `


    };  
})







function previewFiles() {
    let preview = document.querySelector("#preview")
    let files = document.querySelector("input[type=file]").files
    let reminder = document.querySelector("#reminder")
    let maxNoFiles = 6
    if (files.length <= maxNoFiles - preview.childElementCount) {
        if (reminder.innerHTML != "") {
            console.log(reminder)
            reminder.innerHTML = ""
        }

        function readAndPreview(file) {
            // Make sure `file.name` matches our extensions criteria
            if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
                let reader = new FileReader()

                reader.addEventListener(
                    "load",
                    function () {
                        let image = new Image()
                        image.title = file.name
                        image.src = this.result
                        image.className+='uploaded-img'
                        preview.appendChild(image)
                    },
                    false
                )

                reader.readAsDataURL(file)
            }
        }

        if (files) {
            
            [].forEach.call(files, readAndPreview)
        }
    } else {
        if (reminder.childElementCount === 0) {
            reminder.innerHTML = `*  .maximum photos are ${maxNoFiles} .`
        }
    }
}

window.onload = () => {
    checkUserLogin()

}




async function checkUserLogin() {
    const res = await fetch("/current-user");
    const userInfo = await res.json();
    const loginButton = document.querySelector("#nav-bar");
    const uploadSection = document.querySelector("#upload-section");

    if (userInfo.username) {

        login_icon.classList.remove('display-none')
        logout_icon.classList.add('display-none')
        top_bar.classList.remove('display-none')
        logo_banner.classList.add('display-none')
        upload_container.classList.remove('display-none')
        
    }else{
        upload_container.classList.add('display-none')
        start_container.onclick =()=>{
            top_bar.classList.remove('display-none')
            logo_banner.classList.add('display-none')
            loginModal.show()
        }

        login_modal.addEventListener('hidden.bs.modal',()=>{
            top_bar.classList.add('display-none')
            logo_banner.classList.remove('display-none')
            modal_body.classList.add('display-none')
            register_btn.classList.remove('active')
            login_btn.classList.remove('active')
        })
    }
}



    

    
     



    
