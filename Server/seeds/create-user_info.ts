import { Knex } from "knex";
import { hashPassword } from "../utils/hash";

const currency_category = [
  { category: "EURO5", currency: "EURO", face_value: 5 },
  { category: "EURO10", currency: "EURO", face_value: 10 },
  { category: "EURO20", currency: "EURO", face_value: 20 },
  { category: "EURO50", currency: "EURO", face_value: 50 },
  { category: "GBP5", currency: "GBP", face_value: 5 },
  { category: "GBP10", currency: "GBP", face_value: 10 },
  { category: "GBP20", currency: "GBP", face_value: 20 },
  { category: "HKD10", currency: "HKD", face_value: 10 },
  { category: "HKD20", currency: "HKD", face_value: 20 },
  { category: "HKD50", currency: "HKD", face_value: 50 },
  { category: "HKD100", currency: "HKD", face_value: 100 },
  { category: "HKD500", currency: "HKD", face_value: 500 },
  { category: "HKD1000", currency: "HKD", face_value: 1000 },
  { category: "CNY1", currency: "CNY", face_value: 1 },
  { category: "CNY10", currency: "CNY", face_value: 10 },
  { category: "CNY20", currency: "CNY", face_value: 20 },
  { category: "CNY50", currency: "CNY", face_value: 50 },
  { category: "CNY100", currency: "CNY", face_value: 100 },
  { category: "JPY1000", currency: "JPY", face_value: 1000 },
  { category: "JPY5000", currency: "JPY", face_value: 5000 },
  { category: "JPY10000", currency: "JPY", face_value: 10000 },
  { category: "KRW1000", currency: "KRW", face_value: 1000 },
  { category: "KRW5000", currency: "KRW", face_value: 5000 },
  { category: "KRW10000", currency: "KRW", face_value: 10000 },
  { category: "THB20", currency: "THB", face_value: 20 },
  { category: "THB500", currency: "THB", face_value: 500 },
  { category: "THB1000", currency: "THB", face_value: 1000 },
  { category: "TWD100", currency: "TWD", face_value: 100 },
  { category: "TWD200", currency: "TWD", face_value: 200 },
  { category: "TWD1000", currency: "TWD", face_value: 1000 },
  { category: "NOT_CURRENCY", currency: "NOT_CURRENCY", face_value: 0 },
];

export async function seed(knex: Knex): Promise<void> {
  await knex("cash_result").del();
  await knex("cash_image").del();
  await knex("cash_category").del();
  await knex("user_info").del();

  await knex
    .insert([
      {
        emailaddress: "mark@tecky.io",
        password: await hashPassword("mark"),
      },
      {
        emailaddress: "kate@tecky.io",
        password: await hashPassword("kate"),
      },
      {
        emailaddress: "wayne@tecky.io",
        password: await hashPassword("wayne"),
      },
    ])
    .into("user_info")
    .returning("*");

  let cash_category_results = await knex.from("cash_category").max("id");
  let cash_category = cash_category_results[0]["max"];
  if (cash_category === null) {
    for (let i = 0; i < currency_category.length; i++) {
      await knex
        .insert({
          currency: currency_category[i]["currency"],
          category: currency_category[i]["category"],
          face_value: currency_category[i]["face_value"],
        })
        .into("cash_category")
        .returning("*");
    }
  }
}
