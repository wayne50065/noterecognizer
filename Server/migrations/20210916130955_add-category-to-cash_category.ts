import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

    return knex.schema.alterTable('cash_category',table=>{
        table.string('category');
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.alterTable('cash_category',table=>{
        table.dropColumn('category');
    });
}

