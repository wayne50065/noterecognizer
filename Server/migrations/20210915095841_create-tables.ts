import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('user_info',table=>{
        table.increments();
        table.string('emailaddress').notNullable();
        table.string('password').notNullable();
        table.timestamps(false,true);
    })

    await knex.schema.createTable('cash_category',table=>{
        table.increments();
        table.string('currency').notNullable();
        table.string('face_value').notNullable();
        table.timestamps(false,true);
    })

    await knex.schema.createTable('cash_image',table=>{
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references("user_info.id");
        table.string('cash_image');
        table.integer('batch_no');
        table.timestamps(false,true);
    })

    await knex.schema.createTable('cash_result',table=>{
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references("user_info.id");
        table.integer('cash_image_id').unsigned();
        table.foreign('cash_image_id').references("cash_image.id");
        table.integer('cash_category_id').unsigned();
        table.foreign('cash_category_id').references("cash_category.id");
        table.integer('quantity');
        table.float('accuracy');
        table.timestamps(false,true);
    })
    

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('cash_result');
    await knex.schema.dropTable('cash_image');
    await knex.schema.dropTable('cash_category');
    await knex.schema.dropTable('user_info');

}

