// type / interface /data type

export interface User {
    emailaddress: string;
    password: string;

  }
  
  export interface RegisterForm {
    emailaddress: string;
    password: string;

  }
  

  export interface max_batch_no {
    batch_no: number;
  }



  export interface CashAllData {
    batch_no: number;
    emailaddress: string;
    user_id:string;
    face_value:number;
    quantity:number;
    cash_image:string;
    currency:string;
    images_create_at:Date;
    images_update_at:Date;
    result_create_at:Date;
    result_update_at:Date
  }

  export interface AiResult {
    imageName: string;
    class: string;
    probability:string
  }

