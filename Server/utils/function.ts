export function getImageBase64(file:any):Promise<string | ArrayBuffer> {
    return new Promise(function (resolve, reject) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        resolve(reader.result);          
      };
      
      reader.onerror = function (error) {
        console.log("Error: ", error);
        reject(error);
      };
    });
  };