// import { Client } from "pg";
import { CashService } from "../service/CashService";
import { CashController } from "./CashController";
import { Request, Response } from "express";
import {Knex} from "knex";

describe("CashController", () => {
  let cashController: CashController;
  let req: Request;
  let res: Response;
  let cashService: CashService;

  beforeEach(() => {
    cashService = new CashService({} as Knex);
    jest.spyOn(cashService, "createCashRecord").mockImplementation(async () => {});
    // jest.spyOn(cashService,'updateCashRecord').mockImplementation(async ()=>Promise.resolve([{content:"1234",id:1}]));
    // jest.spyOn(cashService,'axios').mockImplementation(async ()=>Promise.resolve([{"imageName": "image_id-1631853034580.jpeg","imageBase64": "/9j/4AAQSk"}]));

    jest.spyOn(cashService,'getCashRecord').mockImplementation(async ()=>Promise.resolve([{ emailaddress: "1234", userid: 1, batch_no: 2, accuracy: 3, currency: "HKD", face_value: 10, quantity: 1}]));
    
    cashController = new CashController(cashService);


    res = {
      json: jest.fn(() => null),
    } as any as Response;
  });

  it("should insert cash", async () => {
    req = {
      file: {
        filename: "HKD10.jpg"
      },
    } as Request;

    await cashController.insertCashRecords(req, res);
    expect(cashService.createCashRecord).toBeCalledWith("HKD10.jpg");
    expect(res.json).toBeCalledWith({ success: true });

  });
  
    it("should update cash result", async () => {
        req = {
          params: {
            user_id: 1,
            category_id:1,
            cash_image_id:1,
          },
          body: {
            quantity:1,
            accuracy: 0.95,
          },
        } as any as Request;
  
        await cashController.getCashRecords(req, res);
  
        expect(cashService.updateCashRecord).toBeCalledWith(1,1,1,1,0.95);
        expect(res.json).toBeCalledWith({ success: true });
      });



    it.only("should get cash records", async () => {
      cashController.getCashRecords
      req = {
        session: {
          user:{
            emailAddress: ""}
        }
        // body: {
        //     emailaddress: "address"
        // }
      } as any as Request;

   
      await cashController.getCashRecords(req, res);

   
      expect(cashService.getCashRecord).toBeCalledWith("address");
      expect(res.json).toBeCalledWith([{ emailaddress: "1234", userid: 1, batch_no: 2, accuracy: 3, currency: "HKD", face_value: 10, quantity: 1}]);
    });


});
``