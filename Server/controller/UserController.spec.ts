import {Knex} from "knex";
import { UserService } from "../service/UserService";
import { UserController } from "./UserController";
import { Request, Response } from 'express';

import {hashPassword, checkPassword} from "../utils/hash"
jest.mock("../utils/hash")


describe('UserController',()=>{

    let userController:UserController;
    // let hashed: hashPassword;
    let req: Request;
    let res: Response;
    let userService:UserService;

    beforeEach(()=>{
        
        userService = new UserService({} as Knex);
        jest.spyOn(userService,'createUser').mockImplementation(async ()=>Promise.resolve(null));
        jest.spyOn(userService,'getUsers').mockImplementation(async ()=>Promise.resolve([{ emailaddress: "address", password: "password" }]));

        userController = new UserController(userService);

        res = {
            status: jest.fn(() => res),
            json: jest.fn(() => null),
            redirect:jest.fn(()=>null)
        } as any as Response;


    })


    it("should register",async ()=>{
        userController.register
        req = {
            session: {},
            body: {
                emailaddress: "address 123",
                password: "mark"
            }
        } as Request;
        jest.spyOn(userService,'getUsers').mockImplementation(async ()=>Promise.resolve([]));
        (hashPassword as jest.Mock).mockResolvedValue("password")

        await userController.register(req, res);

        expect(userService.createUser).toBeCalled()
        expect(userService.createUser).toBeCalledWith("address 123", "password")
        expect(res.json).toBeCalledWith({success:true});
    });


    it("should login",async ()=>{
        userController.login
        req = {
            session: {},
            body: {
                emailaddress: "address",
                password: "mark"
            }
        } as Request;
        (checkPassword as jest.Mock).mockResolvedValue(true)

        await userController.login(req, res);


        expect(userService.getUsers).toBeCalledWith("address");
        expect(res.json).toBeCalledWith({ success:true });
    })


    it("should get current user",async ()=>{
        userController.getCurrentUser
        req = {
            session: {User: ""}
        } as any as Request;
        // req.session["User"]="emailaddress"

        await userController.getCurrentUser(req, res);

        // expect(userService.getUsers).toBeCalledWith("");
        expect(res.json).toBeCalledWith({userEmailAddress: ""});
    })


    it("should logout",async ()=>{
        userController.logout
        req = {
            session: {User: ""}
        } as any as Request;

        await userController.getCurrentUser(req, res);


        expect(res.redirect).toBeCalledWith("/index.html");
    })

});
