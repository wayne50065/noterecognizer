import {Request,Response} from 'express';
import dotenv from 'dotenv';
import { hashPassword } from '../utils/hash';
import { User } from "../utils/interface";
import { checkPassword } from "../utils/hash";
import { UserService } from '../service/UserService';

dotenv.config();


export class UserController{

    constructor(private userService:UserService){}


    register = async (req:Request, res:Response) =>{
        const emailaddress = req.body.emailaddress;
        const users:User[] = await this.userService.getUsers(emailaddress); //new
        if (users[0]) {//new
         res.status(401).json({ success: false, msg: "Email is already exist!" });}
        else {
            const password = await hashPassword(req.body.password);
            console.log(req.body.password)
            console.log(password)
            await this.userService.createUser(emailaddress, password);
            res.status(200).json({ success: true });}//new
                
    };

    
    getCurrentUser = async (req:Request, res:Response) =>{

        // @ts-ignore
        let userEmailAddress:any= '';
        if(req.session["user"]){
            res.json({
             // @ts-ignore
            username: req.session["user"].emailaddress
            });
            userEmailAddress = req.session["user"].emailaddress;
        }else{
        res.status(401).json({userEmailAddress});
        }
    }
    
    login = async (req:Request, res:Response) =>{
        const { emailaddress, password } = req.body;
        const users:User[] = await this.userService.getUsers(emailaddress);
        const user = users[0];

        // @ts-ignore
        if (user && await checkPassword(password, user["password"])) {

        const { password, ...others } = user;
        // @ts-ignore
        req.session["user"] = { ...others };
        req.session['user'] = user;
        res.status(200).json({ success: true });
        } else {
        res.status(401).json({ success: false, msg: "Username/Password is incorrect!" });
        }
    };
    
    
    logout = async (req:Request, res:Response) =>{
        delete req.session["user"];
        res.redirect("/index.html");
    };
    
  
}