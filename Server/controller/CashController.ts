import { Request, Response } from "express";
import dotenv from "dotenv";
import { CashAllData } from "../utils/interface";
import { CashService } from "../service/CashService";
import axios from "axios";
import fs from "fs";


dotenv.config();

export class CashController {
  constructor(private cashService: CashService) {}

  getCashRecords = async (req: Request, res: Response) => {
    const emailAddress = req.session["user"]["emailaddress"];
    const result = await this.cashService.getCashRecord(emailAddress);
    let CashData: CashAllData[] = result.rows;
    res.json(CashData);
  };


  insertCashRecords = async (req: Request, res: Response) => {
    try {
    const emailAddress = req.session["user"]["emailaddress"];
    const files = req.files as Express.Multer.File[];

    if (!files || !files[0] || !files[0]["filename"]) {
      res.status(400).json({ success: false });
      return 
    }
      await this.cashService.createCashRecord(emailAddress, files);

      let cashImages = []
      for (var i=0; i<files.length; i++){
      
        let Image_TypeBase64 = fs.readFileSync(files[i].path, {encoding: 'base64'});
        let Image_NameBase64 =files[i].filename
        let Image_ObjBase64 ={"imageName":Image_NameBase64,"imageBase64":Image_TypeBase64};
        cashImages.push(Image_ObjBase64);
      
      }
     

    const aiPythonResults:any = await axios({method:'post', url:'http://localhost:8000',maxBodyLength:1000000000000,data:cashImages});
    const aiResults = aiPythonResults.data["results"]    
    await this.cashService.updateCashRecord(emailAddress,aiResults);

    
    res.status(200).json({ success: true, aiResults });
  } catch (error) {
      console.log(error.message);
      
      res.status(500).json({ success: false });
  }
  };


}
