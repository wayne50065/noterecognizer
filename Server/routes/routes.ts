
// import { cashRoutes } from './cashRoutes';
// app.use(cashRoutes);


import express from 'express';
import { userController } from '../app';
import { cashController } from '../app';
import { isLoggedIn } from '../utils/guard';
import { upload } from "../app";
export const routes = express.Router({ mergeParams: true });
const max_no_upload_file = 10;

routes.post("/login", userController.login);
routes.get('/current-user', userController.getCurrentUser);
routes.get("/logout", userController.logout);
routes.post('/registerform', userController.register);
routes.post("/cashForm",isLoggedIn,upload.array("image_id", max_no_upload_file), cashController.insertCashRecords);
routes.get("/cashRecord",isLoggedIn, cashController.getCashRecords);




