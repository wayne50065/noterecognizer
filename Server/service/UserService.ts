
import { Knex } from "knex";

export class UserService{


    constructor(private knex:Knex){}

    async getUsers(emailaddress:string){
        return(await this.knex.select('*').from('user_info').where('emailaddress',emailaddress));
    }


    async createUser(emailaddress:string,password:string){
        return await this.knex.insert({
            emailaddress: emailaddress,
            password: password
        }).into('user_info')
    }

}  