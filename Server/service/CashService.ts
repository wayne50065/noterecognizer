// import { Client } from "pg";
import { Knex } from "knex";

export class CashService {
  constructor(private knex: Knex) {}

  async createCashRecord(emailAddress: string, files: Express.Multer.File[]) {

    let max_batch = await this.knex.from("cash_image").max("batch_no");

    let max_batch_no = max_batch[0]["max"];

    if (max_batch_no === null) {
      max_batch_no = 1;
    } else {
      max_batch_no++;
    }

    for (let file of files) {

      await this.knex.raw(
        `INSERT INTO cash_image (user_id, cash_image, batch_no) 
            VALUES ((SELECT id from user_info WHERE emailaddress = ?),?,?)`,
        [emailAddress, file["filename"], max_batch_no]
      );
    }
  }

  async updateCashRecord(emailAddress: string, aiResults) {


    let user_id_knex = await this.knex.select('id').from('user_info').where('emailaddress',emailAddress);
    const user_id = user_id_knex[0]["id"];



    for (let aiResult of aiResults) {
      let category = aiResult["class"];
      let cash_image = aiResult["imageName"];
      let accuracy = aiResult["probability"];
    

      let cash_image_id_knex = await this.knex.select('id').from('cash_image').where('cash_image',cash_image);
      let cash_image_id = cash_image_id_knex[0]["id"];
      console.log("cash_image_id: " + cash_image_id);
      let category_id_knex = await this.knex.select('id').from('cash_category').where('category',category);
      let category_id = category_id_knex[0]["id"];
      console.log("category_id: " + category_id);


      await this.knex.raw(
        `insert into cash_result (user_id,  cash_category_id, cash_image_id, quantity, accuracy, created_at,updated_at)
          VALUES( ?,?,?,?,?,NOW()::timestamp,NOW()::timestamp)`,
        [user_id, category_id, cash_image_id, 1, accuracy]
      );
    }
  }

  async getCashRecord(emailAddress: string) {
    //return await this.client.query(
    return await this.knex.raw(
      `select 
      user_info.emailaddress as emailaddress,
      cash_image.user_id as userid,
      cash_image.cash_image as cash_image,
      cash_image.batch_no,
      cash_result.accuracy as accuracy,
      cash_category.currency as currency,
      cash_category.face_value as face_value,
      cash_result.quantity as quantity,
      cash_result.created_at as result_created_at
      from cash_image INNER JOIN user_info on cash_image.user_id=user_info.id 
      INNER JOIN cash_result on cash_image.id=cash_result.cash_image_id
      LEFT JOIN cash_category on cash_category.id=cash_result.cash_category_id 
      WHERE user_info.emailaddress = ?
      `,
      [emailAddress]
    );
  }
}
