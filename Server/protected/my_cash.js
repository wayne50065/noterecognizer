window.onload = function () {
    myCash();
    loadcurrentUser();
}
const logoutModal = new bootstrap.Modal(document.querySelector("#logoutModal"), {
    keyboard: false
})
const menu_btn=document.querySelector(".menu-btn-container .menu-btn")
const menu_list=document.querySelector(".drop-down-meun")
menu_btn.onclick=()=>{
    menu_list.classList.toggle('display-none')
}

const login_icon=document.querySelector(".user-loggedin-icon")
const logout_icon=document.querySelector(".user-icon")
async function loadcurrentUser(){
    const res = await fetch("/current-user");
    const userInfo = await res.json();
    const loginButton = document.querySelector("#nav-bar");
    const uploadSection = document.querySelector("#upload-section");
    if (userInfo.username) {

        login_icon.classList.remove('display-none')
        logout_icon.classList.add('display-none')
    }
}
login_icon.onclick=()=>{
    logoutModal.show()
}



//
async function myCash(){

    const cashRecords = await fetch("/cashRecord");
    const cashResults = await cashRecords.json();

    console.log(cashResults);

    // const picNumber = document.querySelector('#picNumber')
    // const accuracyRate = document.querySelector('#accuracyRate')

    const myCash = document.querySelector('.info-cards-container');
    const date_container=document.querySelector('#date');
    // myCash.innerHTML = "";
    // date_container.innerHTML="";
    



    for (let cashResult of cashResults){

        let accuracy = (cashResult.accuracy)*100 +"%";
        myCash.innerHTML +=`
        <div class="info-cards"> 
                <div class="photo-container">
                    <img src="./uploads/${cashResult.cash_image}" class=""/>
                </div>

                <div class="summary-container">
                    <div class="content">
                    <p>
                    Upload Date: 
                    <br> 
                    ${dateFns.format(new Date(cashResult.result_created_at),'MM/DD/YYYY  HH:mm')}
                    </p>
                    <hr>
                    <p>
                    Currency: 
                    <br> 
                    ${cashResult.currency}  ${cashResult.face_value}
                    <p>
                    </p>
                    Accuracy: 
                    <br> 
                    ${accuracy}  
                    </p>
                </div>
        </div>
        



        `
    }    
}
